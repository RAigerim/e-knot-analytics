import requests
import warnings
import sqlalchemy
import pandas as pd
from time import sleep
import urllib
import json
from sqlalchemy import create_engine
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from time import sleep
import datetime
import smtplib
from email.message import EmailMessage
from os import remove


apikey = 'a8747d8d66b947108ab1001f2777deaa'
server = '91.215.139.42'
database = 'UL'
username = 'Aigerim'
password = 'Vfyhgf35%1'
table_name = 'UL_by_days'
SENDER_EMAIL = "rakhimovaigerim@gmail.com"
APP_PASSWORD = "djvowufjaleewxkx"
recipient = ['d.besheyev@eknot.me', 'vitaliy@eknot.me']

url = 'https://data.egov.kz/api/v4/gbd_ul/v1?apiKey=a8747d8d66b947108ab1001f2777deaa&source={%22from%22:0,%22size%22:10000,%22query%22:{%22fuzzy_like_this%22:{%22fields%22:[%22datereg%22],%22like_text%22:%22'

headers = {
    'Host': 'data.egov.kz',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Cookie': 'lttping=1624524813693; _ga=GA1.2.119799985.1611131318; _ym_uid=16111313181057905347; _ym_d=1611131318; egovLang=ru; OPENDATA_PORTAL_SESSION=f0ef007816d35fcffd1ba6c7acbeda4803c47ed1-___AT=7974c2a8f0e554ef0b9822ffe585b67823bf7a85&___TS=1624528417591; cookiesession1=678B76A8HIOPQRSTUVWXYZABCEFGEF57; _gid=GA1.2.1616680045.1624524465; _ym_isad=1; _ym_visorc=w',
    'Upgrade-Insecure-Requests': '1',
    'Cache-Control': 'max-age=0',
}

dtype = {
    'statuskz': sqlalchemy.types.NVARCHAR(),
    'okedru': sqlalchemy.types.NVARCHAR(),
    'id': sqlalchemy.types.NVARCHAR(),
    'statusru': sqlalchemy.types.NVARCHAR(),
    # 'datereg': sqlalchemy.types.DATETIME(),
    'okedkz': sqlalchemy.types.NVARCHAR(),
    'namekz': sqlalchemy.types.NVARCHAR(),
    'nameru': sqlalchemy.types.NVARCHAR(),
    'bin': sqlalchemy.types.NVARCHAR(),
    'director': sqlalchemy.types.NVARCHAR(),
    'addressru': sqlalchemy.types.NVARCHAR(),
    'addresskz': sqlalchemy.types.NVARCHAR()
}

warnings.filterwarnings("ignore")

session = requests.Session()
retry = Retry(connect=3, backoff_factor=0.5)
adapter = HTTPAdapter(max_retries=retry)
session.mount('http://', adapter)
session.mount('https://', adapter)

params = urllib.parse.quote_plus(
    "DRIVER={ODBC Driver 17 for SQL Server};SERVER=" + server + ";DATABASE=" + database + ";UID="
    + username + ";PWD=" + password)

conn = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)


def egov(d):
    response = session.get(
        url + str(d) + '+06:00%22}}}', verify=False,
        headers=headers)
    rjson = response.json()
    df = pd.DataFrame(rjson)
    df = df[df['datereg'].str.contains(d)]
    df['load_date'] = datetime.datetime.today()
    return df

def send_mail_with_excel(recipient_email, subject, content, excel_file):
    msg = EmailMessage()
    msg['Subject'] = subject
    msg['From'] = SENDER_EMAIL
    msg['To'] = recipient_email
    msg.set_content(content)

    with open(excel_file, 'rb') as f:
        file_data = f.read()
    msg.add_attachment(file_data, maintype="application", subtype="xlsx", filename=excel_file)

    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        smtp.login(SENDER_EMAIL, APP_PASSWORD)
        smtp.send_message(msg)


yesterday = datetime.datetime.today()
yesterday = yesterday - datetime.timedelta(days=1)
errors = []

if len(str(yesterday.month)) == 1:
    month = str(0) + str(yesterday.month)
else:
    month = str(yesterday.month)
if len(str(yesterday.day)) == 1:
    day = str(0) + str(yesterday.day)
else:
    day = str(yesterday.day)
date = [str(yesterday.year) + '-' + month + '-' + day]


while len(date) != 0:
    try:
        data = egov(date[0])
        data.to_sql(table_name, conn, if_exists='append', dtype=dtype, index=False)
        print(date[0] + '\t' + str(len(data)))
        excel_file_name = 'UL_' + date[0].replace('-', '_')
        data.to_excel(excel_file_name + '.xlsx', index=False)
        print("отправка email...")
        
        for email in recipient:
            send_mail_with_excel(email, 'Данные ЮЛ за ' + str(date[0]), '', excel_file_name + '.xlsx')
        print("email отправлен")
        
        remove(excel_file_name + '.xlsx')
        date.remove(date[0])
        
    except json.decoder.JSONDecodeError:
        print('JSONDecodeError')
        continue

sleep(5)
