import requests
import warnings
import sqlalchemy
import pandas as pd
import urllib
import json
from sqlalchemy import create_engine
from sqlalchemy.types import NVARCHAR
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from time import sleep
from requests.structures import CaseInsensitiveDict
import random

url = "https://aisgzk.kz/aisgzk/Proxy/aisgzkZem2/MapServer/find?f=json&searchText={}&contains=false&returnGeometry=true&layers={}&searchFields=KAD_NOMER&sr=3857"

table_name = 'kyzylorda_grounds_geometry'
server = '194.39.64.18'
database = 'kazakhstan'
username = 'Aigerim'
password = 'Vfyhgf35%1'
params = urllib.parse.quote_plus(
    "DRIVER={ODBC Driver 17 for SQL Server};SERVER=" + server + ";DATABASE=" + database + ";UID=" + username + ";PWD=" + password)

engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params, encoding="utf-8")

headers = CaseInsensitiveDict()
headers["Accept"] = "*/*"
headers["Accept-Encoding"] = "gzip, deflate, br"
headers["Accept-Language"] = "ru-KZ,ru-RU;q=0.9,ru;q=0.8,en-US;q=0.7,en;q=0.6"
headers["Connection"] = "keep-alive"
headers["Content-Length"] = "0"
headers["Content-Type"] = "application/json"
headers["Host"] = "aisgzk.kz"
headers["Origin"] = "https://aisgzk.kz"
headers["Referer"] = "https://aisgzk.kz/aisgzk/ru/content/maps/"
headers["sec-ch-ua"] = '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"'
headers["sec-ch-ua-mobile"] = "?0"
headers["sec-ch-ua-platform"] = "Windows"
headers["Sec-Fetch-Dest"] = "empty"
headers["Sec-Fetch-Mode"] = "cors"
headers["Sec-Fetch-Site"] = "same-origin"
headers[
    "User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36"
headers["X-Requested-With"] = "XMLHttpRequest"

geom_dtype = {'layerId': NVARCHAR(),
              'layerName': NVARCHAR(),
              'displayFieldName': NVARCHAR(),
              'foundFieldName': NVARCHAR(),
              'value': NVARCHAR(),
              'geometryType': NVARCHAR(),
              'geometry': NVARCHAR(),
              'OBJECTID': NVARCHAR(),
              'Shape': NVARCHAR(),
              'KAD_NOMER': NVARCHAR(),
              'Shape_Length': NVARCHAR(),
              'Shape_Area': NVARCHAR(),
              'KOORDINATED': NVARCHAR()}


def get_geom(kad_num, layer):

    response = requests.get(url.format(str(kad_num), str(layer)), headers=headers).json()
    if len(response['results']) == 0:
        return pd.DataFrame(columns=list(geom_dtype.keys()))

    data = pd.DataFrame(response['results']).drop('attributes', axis=1)
    data['geometry'] = pd.Series(response['results'][0]['geometry']['rings'])
    data = data.join(pd.DataFrame([response['results'][0]['attributes']]))
    return data


query_geom = """
            select replace([Кадастровый номер ru], '-', '') as kadNum, layer from kyzylorda_grounds_info
            where layer is not null
            order by kadNum
            """
geom = pd.read_sql_query(query_geom, engine)

table_query = """ select KAD_NOMER from """ + table_name

df = pd.read_sql_query(table_query, engine)

df.KAD_NOMER = df.KAD_NOMER.astype(str)
geom.kadNum = geom.kadNum.astype(str)

geom = geom[~geom['kadNum'].isin(df['KAD_NOMER'])]

# b = str(input('kadNum: '))
# geom = geom[geom.kadNum > b]

print('len', len(geom))


for i in list(geom.index):
    try:
        # print(info.loc[i, 'kadNum'], info.loc[i, 'layer'])
        data = get_geom(geom.loc[i, 'kadNum'], geom.loc[i, 'layer'])
        sleep(1)

        data.astype(str).to_sql(table_name, engine, if_exists='append', index=False, dtype=geom_dtype)
        print(i, geom.loc[i, 'kadNum'], len(data), str(geom.loc[i, 'layer']))
        # sleep(random.randint(1, 2))
    except json.decoder.JSONDecodeError:
        with open(table_name + '.txt', 'a') as file:
            file.write(str(geom.loc[i, 'kadNum']) + '\n')
        print('JSONDecodeError', str(geom.loc[i, 'kadNum']))
        sleep(1)
        continue
