import requests
import warnings
import sqlalchemy
import pandas as pd
import urllib
import json
from sqlalchemy import create_engine
from sqlalchemy.types import NVARCHAR
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from time import sleep
from requests.structures import CaseInsensitiveDict
import random

url = "https://aisgzk.kz/aisgzk/Index/FindObjInfoForMap?kadNum={}"

table_name = 'kyzylorda_grounds_info'
server = '194.39.64.18'
database = 'kazakhstan'
username = 'Aigerim'
password = 'Vfyhgf35%1'
params = urllib.parse.quote_plus(
    "DRIVER={ODBC Driver 17 for SQL Server};SERVER=" + server + ";DATABASE=" + database + ";UID=" + username + ";PWD=" + password)

engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params, encoding="utf-8")

headers = CaseInsensitiveDict()
headers["Accept"] = "*/*"
headers["Accept-Encoding"] = "gzip, deflate, br"
headers["Accept-Language"] = "ru-KZ,ru-RU;q=0.9,ru;q=0.8,en-US;q=0.7,en;q=0.6"
headers["Connection"] = "keep-alive"
headers["Content-Length"] = "0"
headers["Content-Type"] = "application/json"
headers["Host"] = "aisgzk.kz"
headers["Origin"] = "https://aisgzk.kz"
headers["Referer"] = "https://aisgzk.kz/aisgzk/ru/content/maps/"
headers["sec-ch-ua"] = '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"'
headers["sec-ch-ua-mobile"] = "?0"
headers["sec-ch-ua-platform"] = "Windows"
headers["Sec-Fetch-Dest"] = "empty"
headers["Sec-Fetch-Mode"] = "cors"
headers["Sec-Fetch-Site"] = "same-origin"
headers[
    "User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36"
headers["X-Requested-With"] = "XMLHttpRequest"

info_dtype = {'Кадастровый номер ru': NVARCHAR(),
              'Срок землепользования ru': NVARCHAR(),
              'Площадь (кв.м.) ru': NVARCHAR(),
              'Кадастровая оценка ru': NVARCHAR(),
              'Землепользователи ru': NVARCHAR(),
              'Делимый участок? ru': NVARCHAR(),
              'Предоставленное право kz': NVARCHAR(),
              'Предоставленное право ru': NVARCHAR(),
              'Категория земель kz': NVARCHAR(),
              'Категория земель ru': NVARCHAR(),
              'Целевое назначение kz': NVARCHAR(),
              'Целевое назначение ru': NVARCHAR(),
              'Местоположение kz': NVARCHAR(),
              'Местоположение ru': NVARCHAR(),
              'Ограничения kz': NVARCHAR(),
              'Ограничения ru': NVARCHAR()}


def get_info(kad_num):
    resp = requests.post(url.format(str(kad_num)), headers=headers)
    
    data = pd.DataFrame()
    
    if resp.status_code == 200:
        
        resp = resp.json()
        
        for j in [0, 2, 6, 7, 8, 9]:
            data.loc[i, resp[j]['Key'] + ' ru'] = resp[j]['Name']['Ru']

        for j in [1, 3, 4, 5, 10]:
            data.loc[i, resp[j]['Key'] + ' kz'] = resp[j]['Name']['Kz']
            data.loc[i, resp[j]['Key'] + ' ru'] = resp[j]['Name']['Ru']      
   
    return data


q = """
select replace(cadastre_number, '-', '') as kadNum from grounds_kyzylorda
where cadastre_number != ''
order by kadNum
"""

df = pd.read_sql_query(q, engine)

# b = str(input('kadNum: '))
# df = df[df.kadNum > b]

print('len', len(df))

for i in list(df.index):
    try:
        # print(info.loc[i, 'kadNum'], info.loc[i, 'layer'])
        data = get_info(df.loc[i, 'kadNum'])
        sleep(1)

        data.to_sql(table_name, engine, if_exists='append', index=False, dtype=info_dtype)
        print(i, df.loc[i, 'kadNum'], len(data))
        
    except json.decoder.JSONDecodeError:
        with open(table_name + '.txt', 'a') as file:
            file.write(str(df.loc[i, 'kadNum']) + '\n')
        print('JSONDecodeError', str(df.loc[i, 'kadNum']))
        sleep(1)
        continue
